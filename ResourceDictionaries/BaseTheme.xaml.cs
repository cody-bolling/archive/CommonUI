﻿using Xamarin.Forms.Xaml;

namespace CommonUI.ResourceDictionaries
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseTheme
    {
        public BaseTheme()
        {
            InitializeComponent();
        }
    }
}
