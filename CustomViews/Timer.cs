﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace CommonUI.CustomViews
{
    public class Timer : Grid
    {
        public TimeSpan Time
        {
            get { return (TimeSpan)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }
        public static readonly BindableProperty TimeProperty = BindableProperty.Create
        (
            "Time",
            typeof(TimeSpan),
            typeof(Timer),
            new TimeSpan(0, 0, 0),
            BindingMode.TwoWay,
            propertyChanged: TimePropertyChanged
        );
        private static void TimePropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Timer Timer = (Timer)Bindable;
            Timer.TimeLabel.Text = string.Format("{0:hh\\:mm\\:ss}", (TimeSpan)NewValue);
        }

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }
        public static readonly BindableProperty LabelProperty = BindableProperty.Create
        (
            "Label",
            typeof(string),
            typeof(Timer),
            "Timer",
            propertyChanged: LabelPropertyChanged
        );
        private static void LabelPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Timer Timer = (Timer)Bindable;
            Timer.TimerLabel.Text = (string)NewValue;
        }

        private Label TimeLabel;
        private Label TimerLabel;
        private Image ResetButton;
        private Image PlayButton;
        private bool Play;

        public Timer()
        {
            Time = new TimeSpan(0, 0, 0);
            Play = false;
            Padding = new Thickness(0, 0, 0, 10);
            Margin = 0;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.Start;
            HeightRequest = 105;
            SetDynamicResource(BackgroundColorProperty, "AccentColor1");

            AddContents();
        }

        private void AddContents()
        {
            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = 145 },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
            };

            RowDefinitions = new RowDefinitionCollection
            {
                new RowDefinition { Height = 65 },
                new RowDefinition { Height = 30 }
            };

            TimeLabel = new Label
            {
                Text = string.Format("{0:hh\\:mm\\:ss}", Time),
                Margin = 0,
                FontSize = 50,
                FontFamily = "SFPro_Bold",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand,
                VerticalTextAlignment = TextAlignment.Center
            };
            Grid.SetColumn(TimeLabel, 0);
            Grid.SetColumnSpan(TimeLabel, 3);
            Grid.SetRow(TimeLabel, 0);
            Children.Add(TimeLabel);

            TimerLabel = new Label
            {
                Text = Label,
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["SmallText"],
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            Grid.SetColumn(TimerLabel, 1);
            Grid.SetRow(TimerLabel, 1);
            Children.Add(TimerLabel);

            ResetButton = new Image
            {
                Margin = 0,
                HeightRequest = 30,
                WidthRequest = 30,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start,
                Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons.replay.png")
            };
            ResetButton.GestureRecognizers.Add
            (
                new TapGestureRecognizer
                {
                    Command = new Command(Reset)
                }
            );
            Grid.SetColumn(ResetButton, 0);
            Grid.SetRow(ResetButton, 1);
            Children.Add(ResetButton);

            PlayButton = new Image
            {
                Margin = 0,
                HeightRequest = 30,
                WidthRequest = 30,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons.play.png")
            };
            PlayButton.GestureRecognizers.Add
            (
                new TapGestureRecognizer
                {
                    Command = new Command(PlayPause)
                }
            );
            Grid.SetColumn(PlayButton, 2);
            Grid.SetRow(PlayButton, 1);
            Children.Add(PlayButton);
        }

        private void Reset()
        {
            Play = false;
            Time = new TimeSpan(0, 0, 0);
            PlayButton.Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons.play.png");
        }

        private void PlayPause()
        {
            if (Play)
            {
                Play = false;
                PlayButton.Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons.play.png");
            }
            else
            {
                Play = true;
                PlayButton.Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons.pause.png");
                StartTimer();
            }
        }

        private void StartTimer()
        {
            DateTime StartTime = DateTime.Now.Subtract(Time);

            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (Play)
                {
                    Time = DateTime.Now.Subtract(StartTime);
                }

                return Play;
            });
        }
    }
}
