﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace CommonUI.CustomViews
{
    public class Header : Grid
    {
        public string TitleText
        {
            get { return (string)GetValue(TitleTextProperty); }
            set { SetValue(TitleTextProperty, value); }
        }
        public static readonly BindableProperty TitleTextProperty = BindableProperty.Create
        (
            "TitleText",
            typeof(string),
            typeof(Header),
            null,
            propertyChanged: TitleTextPropertyChanged
        );
        private static void TitleTextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Header Header = (Header)Bindable;
            Header.Title.Text = (string)NewValue;
        }

        public string SubtitleText
        {
            get { return (string)GetValue(SubtitleTextProperty); }
            set { SetValue(SubtitleTextProperty, value); }
        }
        public static readonly BindableProperty SubtitleTextProperty = BindableProperty.Create
        (
            "SubtitleText",
            typeof(string),
            typeof(Header),
            null,
            propertyChanged: SubtitleTextPropertyChanged
        );
        private static void SubtitleTextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Header Header = (Header)Bindable;
            Header.Subtitle.Text = (string)NewValue;
        }

        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }
        public static readonly BindableProperty IconSourceProperty = BindableProperty.Create
        (
            "IconSource",
            typeof(string),
            typeof(Header),
            null,
            propertyChanged: IconSourcePropertyChanged
        );
        private static void IconSourcePropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Header Header = (Header)Bindable;
            string Source = (string)NewValue;

            if (!Source.Contains("."))
            {
                Source = (string)Application.Current.Resources["CommonUIPath"] + ".Icons." + (string)NewValue + ".png";
            }

            Header.IconFrame.Content = new Image
            {
                Margin = 0,
                Source = ImageSource.FromResource(Source),
                HeightRequest = 60,
                WidthRequest = 60,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
        }

        private Frame IconFrame;
        private Label Title;
        private Label Subtitle;

        public Header()
        {
            Margin = 0;
            Padding = 0;
            ColumnSpacing = 5;
            RowSpacing = 0;
            HeightRequest = 110;
            SetDynamicResource(BackgroundColorProperty, "AccentColor1");
            HorizontalOptions = LayoutOptions.Fill;
            VerticalOptions = LayoutOptions.Start;

            AddContents();
        }

        private void AddContents()
        {
            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = 95 },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = 34 },
                new ColumnDefinition { Width = 34 },
                new ColumnDefinition { Width = 34 },
                new ColumnDefinition { Width = 34 },
                new ColumnDefinition { Width = 10 }
            };

            RowDefinitions = new RowDefinitionCollection
            {
                new RowDefinition { Height = 67 },
                new RowDefinition { Height = 6 },
                new RowDefinition { Height = 37 }
            };

            IconFrame = new Frame
            {
                HeightRequest = 80,
                WidthRequest = 80,
                HasShadow = false,
                IsClippedToBounds = true,
                CornerRadius = 40,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                Padding = 0,
                Margin = new Thickness(15, 15, 0, 15)
            };
            IconFrame.SetDynamicResource(BackgroundColorProperty, "AccentColor2");
            IconFrame.SetDynamicResource(Frame.BorderColorProperty, "TextColor");
            Grid.SetColumn(IconFrame, 0);
            Grid.SetRow(IconFrame, 0);
            Grid.SetRowSpan(IconFrame, 3);
            Children.Add(IconFrame);

            Title = new Label
            {
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["MainTitleText"],
                Margin = new Thickness(10, 0, 0, 0),
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand
            };
            Grid.SetColumn(Title, 1);
            Grid.SetColumnSpan(Title, 5);
            Grid.SetRow(Title, 0);
            Children.Add(Title);

            Subtitle = new Label
            {
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["SmallText"],
                Margin = new Thickness(10, 0, 0, 0),
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand
            };
            Grid.SetColumn(Subtitle, 1);
            Grid.SetColumnSpan(Subtitle, 5);
            Grid.SetRow(Subtitle, 1);
            Grid.SetRowSpan(Subtitle, 2);
            Children.Add(Subtitle);
        }
    }
}
