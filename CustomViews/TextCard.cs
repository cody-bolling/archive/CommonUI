﻿using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CommonUI.CustomViews
{
    public class TextCard : SwipeView
    {
        public string MainText
        {
            get { return (string)GetValue(MainTextProperty); }
            set { SetValue(MainTextProperty, value); }
        }
        public static readonly BindableProperty MainTextProperty = BindableProperty.Create
        (
            "MainText",
            typeof(string),
            typeof(TextCard),
            null,
            propertyChanged: MainTextPropertyChanged
        );
        private static void MainTextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            TextCard Card = (TextCard)Bindable;
            Card.MainTextLabel.Text = (string)NewValue;
        }

        public string SubText
        {
            get { return (string)GetValue(SubTextProperty); }
            set { SetValue(SubTextProperty, value); }
        }
        public static readonly BindableProperty SubTextProperty = BindableProperty.Create
        (
            "SubText",
            typeof(string),
            typeof(TextCard),
            null,
            propertyChanged: SubTextPropertyChanged
        );
        private static void SubTextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            TextCard Card = (TextCard)Bindable;
            Card.SubTextLabel.Text = (string)NewValue;
        }

        public Command Command
        {
            get { return (Command)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public static readonly BindableProperty CommandProperty = BindableProperty.Create
        (
            "Command",
            typeof(Command),
            typeof(TextCard),
            null,
            propertyChanged: CommandPropertyChanged
        );
        private static void CommandPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            TextCard Card = (TextCard)Bindable;
            Card.CommandGesture.Command = (Command)NewValue;
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create
        (
            "CommandParameter",
            typeof(object),
            typeof(TextCard),
            null,
            propertyChanged: CommandParameterPropertyChanged
        );
        private static void CommandParameterPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            TextCard Card = (TextCard)Bindable;
            Card.CommandGesture.CommandParameter = NewValue;
        }

        private Grid Contents;
        private Frame CardBack;
        private Label MainTextLabel;
        private Label SubTextLabel;
        private TapGestureRecognizer CommandGesture;

        public TextCard()
        {
            HeightRequest = 80;
            BackgroundColor = Color.Transparent;
            Margin = new Thickness(15, 0, 0, 0);
            Padding = 0;

            AddContents();
            AddTapGestures();
        }

        private void AddContents()
        {
            Contents = new Grid
            {
                Margin = 0,
                Padding = 0,
                ColumnSpacing = 0,
                RowSpacing = 0,
                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
                },
                RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition { Height = 45},
                    new RowDefinition { Height = 35}
                }
            };

            CardBack = new Frame
            {
                Margin = new Thickness(0, 0, 15, 0),
                Padding = 0,
                CornerRadius = 10,
                HasShadow = false,
                IsClippedToBounds = true
            };
            CardBack.SetDynamicResource(BackgroundColorProperty, "CardColor");
            CardBack.SetDynamicResource(Frame.BorderColorProperty, "CardBorderColor");
            Grid.SetColumn(CardBack, 0);
            Grid.SetRow(CardBack, 0);
            Grid.SetRowSpan(CardBack, 2);
            Contents.Children.Add(CardBack);

            MainTextLabel = new Label
            {
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["LargeText"],
                Margin = new Thickness(15, 0, 0, 0),
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand
            };
            Grid.SetColumn(MainTextLabel, 0);
            Grid.SetRow(MainTextLabel, 0);
            Contents.Children.Add(MainTextLabel);

            SubTextLabel = new Label
            {
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["SmallText"],
                Margin = new Thickness(15, 0, 0, 0),
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand
            };
            Grid.SetColumn(SubTextLabel, 0);
            Grid.SetRow(SubTextLabel, 1);
            Contents.Children.Add(SubTextLabel);

            Content = Contents;
        }

        private void AddTapGestures()
        {
            TapGestureRecognizer PressedEffectGesture = new TapGestureRecognizer
            {
                Command = new Command(async () => await PressedEffect())
            };

            CommandGesture = new TapGestureRecognizer();

            CardBack.GestureRecognizers.Add(PressedEffectGesture);
            CardBack.GestureRecognizers.Add(CommandGesture);
        }

        private async Task PressedEffect()
        {
            await this.FadeTo(0.7, 0);
            await Task.Delay(150);
            await this.FadeTo(1, 50);
        }
    }
}
