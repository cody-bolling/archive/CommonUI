﻿using System.Linq;
using Xamarin.Forms;

namespace CommonUI.CustomViews.Buttons
{
    public class CustomSwipeItem : SwipeItemView
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly BindableProperty TextProperty = BindableProperty.Create
        (
            "Text",
            typeof(string),
            typeof(CustomSwipeItem),
            "",
            propertyChanged: TextPropertyChanged
        );
        private static void TextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            CustomSwipeItem Button = (CustomSwipeItem)Bindable;
            Button.Label.Text = (string)NewValue;
        }

        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }
        public static readonly BindableProperty IconSourceProperty = BindableProperty.Create
        (
            "IconSource",
            typeof(string),
            typeof(CustomSwipeItem),
            "",
            propertyChanged: IconSourcePropertyChanged
        );
        private static void IconSourcePropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            CustomSwipeItem Button = (CustomSwipeItem)Bindable;
            Button.Icon.Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons." + (string)NewValue + ".png");
        }

        private StackLayout Contents;
        private Label Label;
        private Image Icon;

        public CustomSwipeItem()
        {
            Margin = new Thickness(0, 0, 5, 0);
            Padding = 0;
            IsClippedToBounds = true;
            WidthRequest = 70;

            AddContents();
        }

        private void AddContents()
        {
            Contents = new StackLayout
            {
                Margin = 0,
                Padding = 0,
                IsClippedToBounds = true
            };

            Label = new Label
            {
                Margin = 0,
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["SmallText"],
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.End
            };

            Icon = new Image
            {
                Margin = 0,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.EndAndExpand,
                HeightRequest = 50,
                WidthRequest = 50
            };

            Contents.Children.Add(Icon);
            Contents.Children.Add(Label);

            Content = Contents;
        }
    }
}
