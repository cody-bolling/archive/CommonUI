﻿using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CommonUI.CustomViews.Buttons
{
    public class Button : Frame
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly BindableProperty TextProperty = BindableProperty.Create
        (
            "Text",
            typeof(string),
            typeof(Button),
            "",
            propertyChanged: TextPropertyChanged
        );
        private static void TextPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Button Button = (Button)Bindable;
            Button.Label.Text = (string)NewValue;
        }

        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }
        public static readonly BindableProperty IconSourceProperty = BindableProperty.Create
        (
            "IconSource",
            typeof(string),
            typeof(Button),
            "",
            propertyChanged: IconSourcePropertyChanged
        );
        private static void IconSourcePropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Button Button = (Button)Bindable;
            Button.Icon.Source = ImageSource.FromResource((string)Application.Current.Resources["CommonUIPath"] + ".Icons." + (string)NewValue + ".png");
            Button.Contents.Children.Add(Button.Icon);
        }

        public Command Command
        {
            get { return (Command)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public static readonly BindableProperty CommandProperty = BindableProperty.Create
        (
            "Command",
            typeof(Command),
            typeof(Button),
            null,
            propertyChanged: CommandPropertyChanged
        );
        private static void CommandPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Button Button = (Button)Bindable;
            Button.CommandGesture.Command = (Command)NewValue;
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create
        (
            "CommandParameter",
            typeof(object),
            typeof(Button),
            null,
            propertyChanged: CommandParameterPropertyChanged
        );
        private static void CommandParameterPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            Button Button = (Button)Bindable;
            Button.CommandGesture.CommandParameter = NewValue;
        }

        private StackLayout Contents;
        protected Label Label;
        protected Image Icon;
        private TapGestureRecognizer CommandGesture;

        public Button()
        {
            Margin = 0;
            Padding = 0;
            HasShadow = false;
            IsClippedToBounds = true;
            CornerRadius = 10;
            HeightRequest = 50;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            SetDynamicResource(BackgroundColorProperty, "AccentColor1");

            AddContents();
            AddTapGestures();
        }

        private void AddContents()
        {
            Contents = new StackLayout
            {
                Margin = 0,
                Padding = 0,
                Orientation = StackOrientation.Horizontal,
                IsClippedToBounds = true
            };

            Label = new Label
            {
                Margin = 0,
                Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["SmallBoldText"],
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            Icon = new Image
            {
                Margin = new Thickness(0, 5, 5, 5),
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                HeightRequest = 30,
                WidthRequest = 30
            };

            Contents.Children.Add(Label);
            Content = Contents;
        }

        private void AddTapGestures()
        {
            TapGestureRecognizer PressedEffectGesture = new TapGestureRecognizer
            {
                Command = new Command(async () => await PressedEffect())
            };

            CommandGesture = new TapGestureRecognizer();

            GestureRecognizers.Add(PressedEffectGesture);
            GestureRecognizers.Add(CommandGesture);
        }

        private async Task PressedEffect()
        {
            await this.FadeTo(0.7, 5);
            await Task.Delay(150);
            await this.FadeTo(1, 50);
        }
    }
}
