﻿using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace CommonUI.CustomViews.Buttons
{
    public class HeaderOptionButton : PancakeView
    {
        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }
        public static readonly BindableProperty IconSourceProperty = BindableProperty.Create
        (
            "IconSource",
            typeof(string),
            typeof(HeaderOptionButton),
            null,
            propertyChanged: IconSourcePropertyChanged
        );
        private static void IconSourcePropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            HeaderOptionButton Button = (HeaderOptionButton)Bindable;
            string Source = (string)NewValue;

            if (!Source.Contains("."))
            {
                Source = (string)Application.Current.Resources["CommonUIPath"] + ".Icons." + (string)NewValue + ".png";
            }

            Button.Icon.Source = ImageSource.FromResource(Source);
        }

        public Command Command
        {
            get { return (Command)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public static readonly BindableProperty CommandProperty = BindableProperty.Create
        (
            "Command",
            typeof(Command),
            typeof(HeaderOptionButton),
            null,
            propertyChanged: CommandPropertyChanged
        );
        private static void CommandPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            HeaderOptionButton Button = (HeaderOptionButton)Bindable;
            Button.CommandGesture.Command = (Command)NewValue;
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create
        (
            "CommandParameter",
            typeof(object),
            typeof(HeaderOptionButton),
            null,
            propertyChanged: CommandParameterPropertyChanged
        );
        private static void CommandParameterPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            HeaderOptionButton Button = (HeaderOptionButton)Bindable;
            Button.CommandGesture.CommandParameter = NewValue;
        }

        public bool IsExtended
        {
            get { return (bool)GetValue(IsExtendedProperty); }
            set { SetValue(IsExtendedProperty, value); }
        }
        public static readonly BindableProperty IsExtendedProperty = BindableProperty.Create
        (
            "IsExtended",
            typeof(bool),
            typeof(HeaderOptionButton),
            false,
            propertyChanged: IsExtendedPropertyChanged
        );
        private static void IsExtendedPropertyChanged(BindableObject Bindable, object OldValue, object NewValue)
        {
            HeaderOptionButton Button = (HeaderOptionButton)Bindable;

            if ((bool)NewValue)
            {
                Button.CornerRadius = new CornerRadius(10, 10, 0, 0);
                Button.HeightRequest = 37;
            }
            else
            {
                Button.CornerRadius = 10;
                Button.HeightRequest = 34;
            }
        }

        public bool IsSelected { get; private set; }
        private Image Icon;
        private TapGestureRecognizer CommandGesture;

        public HeaderOptionButton()
        {
            IsSelected = false;
            HeightRequest = 34;
            WidthRequest = 34;
            CornerRadius = 10;
            Margin = 0;
            Padding = 0;
            IsClippedToBounds = true;
            BackgroundColor = Color.Transparent;
            HorizontalOptions = LayoutOptions.Start;
            VerticalOptions = LayoutOptions.Start;

            AddContents();
            AddTapGestures();
        }

        private void AddContents()
        {
            Icon = new Image
            {
                Aspect = Aspect.AspectFit,
                Margin = 2,
                HeightRequest = 30,
                WidthRequest = 30,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start
            };

            Content = Icon;
        }

        private void AddTapGestures()
        {
            TapGestureRecognizer PressedEffectGesture = new TapGestureRecognizer
            {
                Command = new Command(PressedEffect)
            };

            CommandGesture = new TapGestureRecognizer();

            GestureRecognizers.Add(PressedEffectGesture);
            GestureRecognizers.Add(CommandGesture);
        }

        private void PressedEffect()
        {
            if (IsSelected)
            {
                IsSelected = false;
                BackgroundColor = Color.Transparent;
            }
            else
            {
                IsSelected = true;
                SetDynamicResource(BackgroundColorProperty, "AccentColor2");
            }
        }
    }
}
