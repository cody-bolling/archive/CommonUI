﻿using System.Linq;
using Xamarin.Forms;

namespace CommonUI.CustomViews.Buttons
{
    public class LargeButton : Button
    {
        public LargeButton() : base()
        {
            HeightRequest = 70;
            Label.Style = (Style)Application.Current.Resources.MergedDictionaries.ToArray()[0]["LargeText"];
            Icon.Margin = new Thickness(0, 10, 10, 10);
            Icon.HeightRequest = 50;
            Icon.WidthRequest = 50;
        }
    }
}
